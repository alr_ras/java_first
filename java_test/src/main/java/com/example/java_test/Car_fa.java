package com.example.java_test;

public class Car_fa {
    String name,company ,pelaak;
    private String countryName;

    public static  void main(String[] b){
        printAll();
    }

    public Car_fa(String name, String company, String pelaak) {
        this.name = name;
        this.company=company;
        this.pelaak = pelaak;
    }

    public String printResult(){
        return String.format("name: "+name+"\tcompany: "+company+"\tpelaak: "+pelaak+"\tCounty Name: "+getCountryName());
    }

    public void print(Object a) {
        System.out.println(a);
    }

    public void printStar(){
        print("\n**************************************************");
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public static void printAll(){
        SubClass1_Car206 subClass1_car206=new SubClass1_Car206();
        subClass1_car206.set206();
        SubClass2_Car405 subClass2_car405=new SubClass2_Car405();
        subClass2_car405.set405();
        SubClass3_CarPride subClass3_carPride=new SubClass3_CarPride();
        subClass3_carPride.setPride();
        SubClass4_CarM140i subClass4_carM140i=new SubClass4_CarM140i();
        subClass4_carM140i.setM140();
        SubClass5_Car2018TACOMA subClass5_car2018TACOMA=new SubClass5_Car2018TACOMA();
        subClass5_car2018TACOMA.setTacoma();
        SubClass6_Car2018AMG_GT_C_Coupe subClass6_car2018AMG_gt_c_coupe=new SubClass6_Car2018AMG_GT_C_Coupe();
        subClass6_car2018AMG_gt_c_coupe.setAmg();
    }

}
